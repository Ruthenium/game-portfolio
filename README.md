# [Odeum](https://gitlab.com/Ruthenium/game-portfolio/-/tree/master/Odeum)

Odeum is a third person action RPG. It was made in 10 weeks within the context of the [Ubisoft Game Lab Competition](https://montreal.ubisoft.com/en/our-commitments/education/game-lab-competition/). It won the `Best Quality of the “3Cs” (camera, characters, controls)` award.

## Trailer Video

[![Gameplay](https://img.youtube.com/vi/NDMK2PJKjP0/0.jpg)](https://www.youtube.com/watch?v=NDMK2PJKjP0)

## Screenshots

![Fight](/source/images/Odeum/Fight.png)
![Theatre](/source/images/Odeum/Theatre.png)
![Mask](/source/images/Odeum/Mask.png)

# [Super Wave Warriors](https://gitlab.com/Ruthenium/game-portfolio/-/tree/master/Super%20Wave%20Warriors)
Super Wave Warriors is a topdown arcade game and was made in 48h for the [Global Game Jam](https://globalgamejam.org/). It was one of the winners for the Montreal Game Jam (ÉTS) location. Some time was spent after the Game Jam to polish the game.

## Gameplay Video

[![Gameplay](http://img.youtube.com/vi/I1HCzeTMMBE/0.jpg)](https://www.youtube.com/watch?v=I1HCzeTMMBE "Super Wave Warriors")

## Screenshots

![Fight](/source/images/SuperWaveWarriors/Fight.png)
![Select](/source/images/SuperWaveWarriors/Select.png)
![Victory](/source/images/SuperWaveWarriors/Victory.png)

# [Project Spellblade](https://gitlab.com/Ruthenium/game-portfolio/-/tree/master/Project%20SpellBlade)
Project Spellblade is a 2D topdown action game and was made in 48h for the UQAT Game Jam. It won first place for `Best Game Experience`, `Technical Innovation` and `Best Audio`. It also got second place for the `Grand Price`.

## Screenshots

![Environment](/source/images/Spellblade/Environment.png)
![Environment2](/source/images/Spellblade/Environment2.png)

# [Sahara](https://gitlab.com/Ruthenium/game-portfolio/-/tree/master/Sahara)
Sahara is an isometric RTS and was made in 120 hours as part of my academic training.

## Screenshots
![Gameplay](/source/images/Sahara/Gameplay.png)
![Fight](/source/images/Sahara/Fight.png)

# [Project Cerberus](https://gitlab.com/Ruthenium/game-portfolio/-/tree/master/Project%20Cerberus)
Project Cerberus is a 2D sidescroller shooter and was made in 48 for the [McGameJam](https://mcgamejam.ca/).

## Screenshots
![Environment](/source/images/Cerberus/Environment.png)
![Fight](/source/images/Cerberus/Fight.png)

